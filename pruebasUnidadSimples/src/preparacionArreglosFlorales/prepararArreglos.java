/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preparacionArreglosFlorales;

/**
 *
 * @author LENOVO
 */
public class prepararArreglos {
    
    public int[] calcularPersonasTiempoParaArreglos(int n){   
        int [] resultado = new int[2];
        int personas=0;
        if(n<20){
            personas =1;
        }else if(n>=20 && n<=49){
            personas = 2;
        }else if(n >= 50){
            personas = (n/25)+1;            
        }
        
        resultado[0] = personas;
        resultado[1] = n*15;        
        return resultado;
    }
    
}
