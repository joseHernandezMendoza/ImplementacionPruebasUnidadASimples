/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preparacionArreglosFlorales;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author LENOVO
 */
public class prepararArreglosTest {
    
public static prepararArreglos instance;
    
    public prepararArreglosTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
        instance = new prepararArreglos();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of calcularPersonasTiempoParaArreglos method, of class prepararArreglos.
     */
    @Test
    public void testCalcularPersonasTiempoParaArreglos() {
        System.out.println("calcular Personas para preparar arreglos");
        int n = 50;
        int[] expResult = (new int[] {3, 750} );
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        System.out.println("Personas: "+result[0]);
        System.out.println("tiempo: "+result[1]);
        assertArrayEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }
    
    /**
     * Test que no regrese un arreglo vacio.
     */
    @Test
    public void valorDeRegresoNoNulo() {
        System.out.println("Prueba de arreglo vacio");
        int n = 0;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        assertNotNull(result);
    }    
    
    /**
     * Test que no regrese un arreglo vacio.
     */
    @Test
    public void numeroDeArreglosValidos() {
        System.out.println("numero de arreglos validos");
        int n = 0;
        int[] result = instance.calcularPersonasTiempoParaArreglos(n);
        if(n > 0){
            fail("El valor es cerro");
        }
    }
    
    
}
